require "gosu"

#YOU FINISHED AT PG 36. NEED TO DEFINE @START TIME
#AND KEEP TRACK OF IT 

class WhackARuby < Gosu::Window 

   def initialize(width, height)
      @screen_width = width
      @screen_height = height
      super(@screen_width, @screen_height)
      self.caption = "Whack the Ruby!"
      @image = Gosu::Image.new("ruby.png")
      @hammer_image = Gosu::Image.new("hammer.png")
      @x = 200
      @y = 200
      @width = 50
      @height = 43
      @velocity_x = 4
      @velocity_y = 4
      @visible = 0
      @hit = 0
      @score = 0
      @playing = true
   end

   #draw images
   def draw()
      #draw diamond, conditioned on its visibility
      if (@visible > 0)
         @image.draw(@x - @width/2, @y - @height/2, 1)
      end
      #draw hammer
      @hammer_image.draw(mouse_x - 40, mouse_y - 10, 1)

      #flash color screen when diamond was hit/missed
      if (@hit == 0)
         c = Gosu::Color::NONE         
      elsif @hit == 1
         c = Gosu::Color::GREEN
      elsif @hit == -1
         c = Gosu::Color::RED
      end
      draw_quad(0, 0, c, 800, 0, c, 800, 600, c, 0, 600, c)
      @hit = 0
      @font = Gosu::Font.new(30)
      
      @font.draw(@score.to_s, 700, 20, 2)

      #Display time remaining.
      @font.draw(@time_left.to_s, 600, 20, 2)
      
      #display game over message
      unless @playing
         @font.draw('Game Over', 300, 300, 3)
         @font.draw('Press the Space Bar to Play Again', 175, 350, 3)
         @visible = 20 
      end
   end
   #upade position of the Ruby Diamond
   def update()
      if @playing
         @x += @velocity_x
         @y += @velocity_y

         #Prevent the ruby from exceeding the screen width/height
         if (@x + @width / 2 > @screen_width || @x - @width / 2 < 0)
            @velocity_x *= -1
         end

         if (@y + @height / 2 > @screen_height || @y - @height / 2 < 0)
            @velocity_y *= -1
         end

         #make the ruby diamond visible on random bases
         @visible -= 1
         if (@visible < -10 && rand < 0.01)
            @visible = 30
         end
         @time_left = (100 - (Gosu.milliseconds / 1000))
         @playing = false if @time_left < 0
      end
   end

   #control mouse click
   def button_down(id)
      #was the ruby hit with the hammer. update score
      if @playing
         if (id == Gosu::MsLeft)
            if (Gosu.distance(mouse_x, mouse_y, @x, @y) < 50 && @visible >= 0)
               @hit = 1
               @score += 5
            else
               @hit = -1
               @score -= 1
            end
         end         
      end
   end
end #class end 

window = WhackARuby.new(800,600)
window.show

